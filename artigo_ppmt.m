close all;clear;

%DMS/PPMT Parameters -- Start
out_size_x = 48;
out_size_y = out_size_x;
grid_size = 0.05;
num_of_sims = 3;
run_method = 'ppmt'; %or 'dms'
subsample_variogram_percent = 1.0;
%DMS/PPMT Parameters -- End

%Load Experimental Data and Cond Points-- Start
ref_logs = read_eas('data.dat');
grid_ = ref_logs(:,[1 2])/10-repmat([1 0],[size(ref_logs,1) 1]);
logs = ref_logs(:,[3 4]);
reference_models = zeros(size(logs,2),max(grid_(:,1)),max(grid_(:,2)));
reference_models(1,:,:) = reshape(logs(:,1),max(grid_(:,1)),max(grid_(:,2)));
reference_models(2,:,:) = reshape(logs(:,2),max(grid_(:,1)),max(grid_(:,2)));

load('conditioning_indexes.mat');
%index_cond = randi(48^2,1,100);
cond_value =  logs(index_cond,:);
cond_pos = grid_(index_cond,[1 2]);
%Load Experimental Data and Cond Points-- Stop% 

%Variogram Fitting for Range Estimation-- Start
figure;
simulation_ranges = [];
grid_size_transf = 0.05;
gauss_conds = [];
n_vars = size(logs,2);
for var_id = 1:n_vars
    gauss_cond_i = fast_joint_ressample_no_normalization_back(ref_logs(index_cond,2+var_id),ref_logs(:,2+var_id),grid_size_transf,1000);
    gauss_conds = [gauss_conds,gauss_cond_i];
    % Calcula variograma
    subplot(1,n_vars+1,var_id);
    variogram_field_exp = variogram(grid_(index_cond,[1 2]), gauss_cond_i,'plotit',true,'maxdist',40);
    % fita variograma
    [~,~,~,vstruct_field] = variogramfit(variogram_field_exp.distance,variogram_field_exp.val,[],[],[],'model','spherical','color','k');
    hold on
    simulation_ranges = [simulation_ranges;vstruct_field.range,vstruct_field.range];
    legend('Experimental','Model')
    pause(0.001);
end
subplot(1,3,1)
xlabel('\Delta z^1')
subplot(1,3,2)
xlabel('\Delta z^2')
%subplot(1,n_vars+1,n_vars+1)
%crossvariogram_field = cross_variogram(grid_(index_cond,[1 2]), gauss_conds(:,1),gauss_conds(:,2),'plotit',false,'maxdist',40);
%[~,~,~,vstruct_field] = variogramfit(variogram_field_exp.distance,variogram_field_exp.val,[],[],[],'model','spherical','color','k');
hold on
%Variogram Fitting for Range Estimation-- Stop
%Run both Methods -- Start
logs_simulated_all_ppmt = cell(num_of_sims,1);
for i = 1:1:num_of_sims
    logs_simulated = run_sim_ppmt([max(grid_(:,1)),max(grid_(:,2))],out_size_x,out_size_y, simulation_ranges, logs, cond_pos,cond_value);
    logs_simulated_all_ppmt{i} = logs_simulated;
end
logs_simulated_all_dms = dms_cond(out_size_x,out_size_y,  simulation_ranges(:,1), grid_size, logs, cond_pos, cond_value, num_of_sims);


if isequal(run_method,'ppmt')
    logs_simulated_all = logs_simulated_all_ppmt;
else
    logs_simulated_all = logs_simulated_all_dms;
end
%Run both Methods -- Stop

%Calculate Variances -- Start
for b = 1:1:size(logs_simulated_all{1},1)
    for c = 1:1:size(logs_simulated_all{1},2)
        for d = 1:1:size(logs_simulated_all{1},3)
            V = [];
            for a = 1:1:size(logs_simulated_all,1)
                v = logs_simulated_all{a}(b,c,d);
                V(end+1) = v;
            end
            logs_simulated_var(b,c,d) = var(V);
        end
    end
end
%Calculate Variances -- End
 
plot_reference_model
generate_2D
generate_2D_var
generate_biplots(logs_simulated(:,:)');
generate_biplots(logs);
generate_variograms_bivariate