%%%%%%% 
% 
% MATLAB R2018a
%%%%%%%
%% %%%%% CLEAN WORKSPACE AND PREVIOUS SIMULATION FILES %%%%%
close all;
clc;clear;
% if exist('Source Code/Library/Third Party/ppmt/par/','dir');rmdir('Source Code/Library/Third Party/ppmt/par/','s');mkdir('Source Code/Library/Third Party/ppmt/par/');else mkdir('Source Code/Library/Third Party/ppmt/par/');end 
% if exist('Source Code/Library/Third Party/ppmt/data/','dir');rmdir('Source Code/Library/Third Party/ppmt/data/','s');mkdir('Source Code/Library/Third Party/ppmt/data/');else mkdir('Source Code/Library/Third Party/ppmt/data/');end 
% if exist('sgsim.dbg');delete('sgsim.dbg');end


%% %%%%% MAIN CONFIGURATION OF SIMULATIONS %%%%%
dx_sim = 50;
dy_sim = 50;
simulation_ranges = [10 10];
grid_size = 0.05; %DMS

%% %%%%% DATA PREPARATION %%%%%
dx_ref = 200;
dy_ref = 200;
range_ref = 20;

% main_2D_LE
load('reference_data.mat');
reference_logs = [z1_analytic, z2_analytic, z3_analytic, z4_analytic, z5_analytic, z6_analytic];

%% %% %%%%% RUN DMS %%%%%
% tic
% logs_simulated_dms = dms_cond(dx_sim, dy_sim, simulation_ranges(1), grid_size, reference_logs, [] , [],1);
% time_dms = toc;
% clearvars -except z1_analytic z2_analytic z3_analytic z4_analytic z5_analytic z6_analytic time_ppmt dx_sim dy_sim dx_ref dy_ref simulation_ranges time_dms time_ppmt logs_simulated_dms

%% %%%%% RUN PPMT %%%%%
tic
logs_simulated_ppmt = run_sim_ppmt([dx_ref,dy_ref],dx_sim,dy_sim, repmat(simulation_ranges,[6 1]), reference_logs, [],[]);
logs_simulated_ppmt_temp = zeros(dx_sim*dy_sim,size(reference_logs,2));
for log_id = 1:size(logs_simulated_ppmt,1)
    
    logs_simulated_ppmt_temp(:,log_id) = reshape(logs_simulated_ppmt(log_id,:,:),[dx_sim*dy_sim 1]);
end
time_ppmt = toc;
%% %%%%% DRAW RESULTS AND COMPARISONS
draw_results_ppmt_dms;