close all

%Load Experimental Data -- Start
main_nD
logs= [z1_analytic, z2_analytic, z3_analytic, z4_analytic, z5_analytic, z6_analytic];
%Load Experimental Data -- End

%Crossplot Experimental -- Start
z1_analytic = logs(:,1);
z2_analytic = logs(:,2);
z3_analytic = logs(:,3);
z4_analytic = logs(:,4);
z5_analytic = logs(:,5);
z6_analytic = logs(:,6);
generate_crossplots_leandro; %Generate Experimental Biplots
%Crossplot Experimental -- End

%DMS -- Start
%DMS Parameters -- Start
out_size_x = 100;
out_size_y = out_size_x;
range = 1;
grid_size = 0.05;
num_of_sims = 1;
%DMS Parameters -- End

logs_simulated_all = cell(num_of_sims,1);
tic
for i = 1:1:num_of_sims
    logs_simulated = dms(out_size_x,out_size_y, range, grid_size, logs, [] , [],[]);
    logs_simulated_all{i} = logs_simulated;
end
%DMS -- End
toc
z1_analytic = logs_simulated(1,:);
z2_analytic = logs_simulated(2,:);
z3_analytic = logs_simulated(3,:);
z4_analytic = logs_simulated(4,:);
z5_analytic = logs_simulated(5,:);
z6_analytic = logs_simulated(6,:);

generate_crossplots_leandro %Generate Simulated Biplots
generate_2D %Generate Simulated Plot 2D Values
