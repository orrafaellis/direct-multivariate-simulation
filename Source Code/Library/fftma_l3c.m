function [ noises ] = fftma_l3c(m,n, range_v, range_h, err, root_noises)
%FFTMA_L3C Summary of this function goes here
%   Detailed explanation goes here
%FFTMA
noises = root_noises;
noises = reshape(noises,m,n);
[correlation_function] = construct_correlation_function(range_v,range_h,noises,3,0);
noises = FFT_MA_3D(correlation_function,noises);
%FFTMA

noises = reshape(noises, m , n);
noises = gaussianator(noises(:));
noises = noises - min(min(noises));
noises = noises ./ max(max(noises));
noises = reshape(noises,m,n);
noises = noises - repmat(min(noises(:)),size(noises,1),size(noises,2));
noises = noises ./ repmat(max(noises(:)),size(noises,1),size(noises,2));
% noises(50,:)
if err
    
    
    noise = noises(:);
    noise = noise - min(noise);
    noise = noise ./ max(noise);

    

    %uniformiza
    logs_cumhist = unique(sort(noise));
    logs_cumhist_x = [1:1:size(logs_cumhist,1)]' -1; 
    logs_cumhist_x = logs_cumhist_x ./ max(logs_cumhist_x);
    noise2 = interp1(logs_cumhist, logs_cumhist_x, noise, 'cubic'); %amostra
    noises = reshape(noise2,m,n);
end

end

