x = log_i;
if direction == 'x' x = reshape(x',1,size(x,1)*size(x,2));
else x = x(:,1);   
end
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

h.XLim = [0,50];
h.YLim = [-1,1];
