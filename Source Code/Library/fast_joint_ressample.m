function [ logs_simulated, inlogs ] = fast_joint_ressample( inlogs, outlogs, grid_size, logs_raw)
%inlogs: dado para ser reamostrado, pode ser um ru�do gaussiano saindo
%diretamente do fftma MxN
%outlogs: Dado que deve ter as distribui��es locais honradas.
% grid_size: tamanho da vizinhan�a

if size(logs_raw,1) == 0
    logs_raw = -999 * ones(size(inlogs));
end



outlogs0 = outlogs; %copia os dados de netrada;
outlogs = outlogs - repmat(min(outlogs), size(outlogs,1), 1); %normaliza
mm = max(outlogs);
outlogs = outlogs ./ repmat(max(outlogs), size(outlogs,1), 1);


index = logs_raw == -999;
logs_raw0 = logs_raw;
logs_raw = logs_raw - repmat(min(outlogs0), size(logs_raw,1), 1); %normaliza
logs_raw = logs_raw ./ repmat(mm, size(logs_raw,1), 1);
logs_raw(index) = -999;


for i = 1:1:size(inlogs,2)
    logs_cumhist = sort(inlogs(:,i));
    ind = round((inlogs(i).*(size(logs_cumhist,1)-1) + 1));
    sub_logs = logs_cumhist(ind);
  end


logs_simulated = zeros(size(inlogs));
logs_sub2 = 8;

for i = 1:1:size(inlogs,1) %para cada ru�do repete;
    logs_target = outlogs;
    nois = inlogs(i,:);
    
    for j =1:1:size(nois,2); %para cada log repete
        
        logs_sub = sort(logs_target(~isnan(logs_target(:,1)),j));  %gera cumulativa
           if size(logs_sub,1) > 1
              logs_cumhist = logs_sub;
              ind = round((nois(j).*(size(logs_cumhist,1)-1) + 1));
              logs_sub2 = logs_cumhist(ind);
           else
               logs_sub2 = logs_sub;
           end
           
            rr = logs_raw(i,j);
            if rr ~= -999
%                 index = knnsearch(logs_cumhist,rr);
%                 logs_sub2 = logs_cumhist(index);
                logs_sub2 = rr;
            end
        try
            nois(j) = logs_sub2;
        catch e
            k = 0 ;
        end
       
%        index0 = knnsearch(logs_target(:,j), logs_sub2, 'k', 50);
%        index = zeros(size(logs_target,1),1);
%        index(index0) = 1; 

       index = abs(logs_target(:,j) - logs_sub2) < grid_size; %filtra apenas valores em torno do valor amostrado     
       sum(index);
       logs_target(~index,:) = NaN;
    end
    logs_simulated(i,:) = nois;
end

logs_simulated = logs_simulated - repmat(min(logs_simulated),size(logs_simulated,1),1); %D� uma �ltima normalizada nos resultados
logs_simulated = logs_simulated ./ repmat(max(logs_simulated),size(logs_simulated,1),1);

logs_simulated = logs_simulated .* repmat(max(outlogs0)-min(outlogs0),size(logs_simulated,1),1); %Coloca resultados nas ordens de grandeza corretas
logs_simulated = logs_simulated + repmat(min(outlogs0),size(logs_simulated,1),1);
index = logs_raw ~= -999;
logs_simulated(index) = logs_raw0(index);
end