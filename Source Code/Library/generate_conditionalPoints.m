%%%%%%% 
% 
% MATLAB R2018a
%%%%%%%
%% %%%%% CLEAN WORKSPACE AND PREVIOUS SIMULATION FILES %%%%%
close all;
clc;clear;
%if exist('Source Code/Library/Third Party/ppmt/par/','dir');rmdir('Source Code/Library/Third Party/ppmt/par/','s');mkdir('Source Code/Library/Third Party/ppmt/par/');else mkdir('Source Code/Library/Third Party/ppmt/par/');end 
%if exist('Source Code/Library/Third Party/ppmt/data/','dir');rmdir('Source Code/Library/Third Party/ppmt/data/','s');mkdir('Source Code/Library/Third Party/ppmt/data/');else mkdir('Source Code/Library/Third Party/ppmt/data/');end 
%if exist('sgsim.dbg');delete('sgsim.dbg');end


%% %%%%% MAIN CONFIGURATION OF SIMULATIONS %%%%%
dx_sim = 100;
dy_sim = 100;
simulation_ranges = [60 60];
grid_size = 0.05; %DMS

%% %%%%% DATA PREPARATION %%%%%
dx_ref = 200;
dy_ref = 200;
range_ref = 20;

% main_2D_LE
load('reference_data.mat');
reference_logs = [z1_analytic, z2_analytic, z3_analytic, z4_analytic, z5_analytic, z6_analytic];

%% %% %%%%% RUN DMS %%%%%
tic
reference_models = dms(dx_sim, dy_sim, simulation_ranges(1), grid_size, reference_logs, [] , [], []);
time_dms = toc;
clearvars -except z1_analytic z2_analytic z3_analytic z4_analytic z5_analytic z6_analytic time_ppmt dx_sim dy_sim dx_ref dy_ref simulation_ranges time_dms time_ppmt reference_models

%% %%%%% DRAW RESULTS AND COMPARISONS
%% DRAW RESULTS
close all;
figure;

L = max(simulation_ranges);

tab_names = {'Reference Distribution','PPMT Distribution',...
    'DMS Distribution','PPMT Simulations','DMS Simulations','PPMT Variogram','DMS Variogram','Time Comparison'};

desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
myGroup = desktop.addGroup('Results');
desktop.setGroupDocked('Results', 0);
myDim   = java.awt.Dimension(4, 2);   
desktop.setDocumentArrangement('Results', 1, myDim); % 1: Maximized, 2: Tiled, 3: Floating

figH    = gobjects(1, length(tab_names));
bakWarn = warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
pause(0.01);
%% REFERENCE DISTRIBUTION
iFig = 1; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002);
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
z1_var = z1_analytic;
z2_var = z2_analytic;
z3_var = z3_analytic;
z4_var = z4_analytic;
z5_var = z5_analytic;
z6_var = z6_analytic;
generate_crossplots_report;
clear z1_var z2_var z3_var z4_var z5_var z6_var;
pause(0.01);

%% 3 - DMS Distribution
if exist('reference_models','var')
    iFig = 3; 
    figH(iFig) = figure('WindowStyle', 'docked', ...
      'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
    drawnow;
    pause(0.002);  
    set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
    if size(reference_models,1)>0; z1_var = reference_models(1,:,:); end
    if size(reference_models,1)>1; z2_var = reference_models(2,:,:); end
    if size(reference_models,1)>2; z3_var = reference_models(3,:,:); end
    if size(reference_models,1)>3; z4_var = reference_models(4,:,:); end
    if size(reference_models,1)>4; z5_var = reference_models(5,:,:); end
    if size(reference_models,1)>5; z6_var = reference_models(6,:,:); end
    generate_crossplots_report;
    pause(0.01);
end

%% Sampling harddata / conditional points
n_points = 200;
x_points = randi(dx_sim,n_points,1);
y_points = randi(dy_sim,n_points,1);
cond_pos = [x_points y_points];
for pto=1:n_points
    for var = 1:size(reference_models,1)
        cond_value(pto,var) = reference_models(var,x_points(pto),y_points(pto));
    end
end

%% 5 - DMS Output
if exist('reference_models','var')
    iFig = 5; 
    figH(iFig) = figure('WindowStyle', 'docked', ...
      'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
    drawnow;
    pause(0.002);  
    set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
    n_vars = size(reference_models,1);
    for p_id = 1:n_vars
        subplot(max(ceil(n_vars/3),1),min(n_vars,3),p_id);
        log_i = reshape(reference_models(p_id,:,:),dx_sim,dy_sim);

        imagesc(log_i);
        daspect([1 1 1])
        xlabel('Y');
        ylabel('X');
        title(['Simulation of Z',num2str(p_id)]);
        
        hold all
        plot(x_points,y_points,'k+')
    end
    pause(0.1);
end





