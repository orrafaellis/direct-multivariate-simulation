
function [ logs_ressampled ] = fast_marginal_ressample(logs, logs_target)

    logs = logs - repmat(min(logs), size(logs,1), 1);
    logs = logs ./ repmat(max(logs), size(logs,1), 1);
    logs_normalized = zeros(size(logs));
    logs_ressampled = zeros(size(logs));

    for i = 1:1:size(logs,2)
        logs_cumulative = sort(logs(:,i));
        logs_normalized(:,i) = knnsearch(logs_cumulative, logs(:,i));
    end

    logs_normalized = logs_normalized - repmat(min(logs_normalized), size(logs_normalized,1), 1);
    logs_normalized = logs_normalized ./ repmat(max(logs_normalized), size(logs_normalized,1), 1);

    logs_normalized = logs_normalized .* (size(logs_target,1) - 1);
    logs_normalized = logs_normalized + 1;
    logs_normalized = round(logs_normalized);

    for i = 1:1:size(logs_target,2)
        logs_cumulative = sort(logs_target(:,i));
        logs_ressampled(:,i) = logs_cumulative(logs_normalized(:,i));
    end

end


