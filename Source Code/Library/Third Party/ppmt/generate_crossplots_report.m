%% Visualização
font_size = 13;

subplot(6,6,1);
histogram(z1_var)
axis square
ax = gca;
ylabel('Z_1', 'FontSize', font_size)
%%title('Z_1','FontSize',font_size,'FontWeight','normal');

%ylabel('Frequency')
grid;
if exist('z2_var','var')
    subplot(6,6,1+7)
    histogram(z2_var)
    axis square
    %title('Z_1','FontSize',font_size,'FontWeight','normal');
    ylabel('Z_2', 'FontSize', font_size)
    grid;
end

if exist('z3_var','var')
    subplot(6,6,1+7*2)
    histogram(z3_var)
    axis square
    ylabel('Z_3', 'FontSize', font_size)
    %title('Z_3','FontSize',font_size,'FontWeight','normal');
    grid
end

if exist('z4_var','var')
    subplot(6,6,1+7*3)
    histogram(z4_var)
    axis square
    ylabel('Z_4', 'FontSize', font_size)
    %title('Z_4','FontSize',font_size,'FontWeight','normal');
    grid;
end

if exist('z5_var','var')
    subplot(6,6,1+7*4)
    histogram(z5_var)
    axis square
    ylabel('Z_5', 'FontSize', font_size)
    %title('Z_5','FontSize',font_size,'FontWeight','normal');
    grid;
end
if exist('z6_var','var')
    subplot(6,6,1+7*5)
    histogram(z6_var)
    axis square
    ylabel('Z_6', 'FontSize', font_size)
    xlabel('Z_6', 'FontSize', font_size)
    %title('Z_6','FontSize',font_size,'FontWeight','normal');
    grid;
end

if exist('z2_var','var')
subplot(6,6,7)
histogram2(z1_var,z2_var,'FaceColor','flat')
axis square
%xlabel('Z_1', 'FontSize', font_size)
ylabel('Z_2','FontSize', font_size)
view([0 90])
else
    return
end

if exist('z3_var','var')
subplot(6,6,13)
histogram2(z1_var,z3_var,'FaceColor','flat')
axis square
%xlabel('Z_1', 'FontSize', font_size)
ylabel('Z_3','FontSize', font_size)
view([0 90])
subplot(6,6,14)
histogram2(z2_var,z3_var,'FaceColor','flat')
axis square
%xlabel('Z_2', 'FontSize', font_size)
ylabel('Z_3','FontSize', font_size)
view([0 90])
else
    return
end

if exist('z4_var','var')
subplot(6,6,19)
histogram2(z1_var,z4_var,'FaceColor','flat')
axis square
%xlabel('Z_1', 'FontSize', font_size)
ylabel('Z_4','FontSize', font_size)
view([0 90])
subplot(6,6,20)
histogram2(z2_var,z4_var,'FaceColor','flat')
axis square
%xlabel('Z_2', 'FontSize', font_size)
ylabel('Z_4','FontSize', font_size)
view([0 90])
subplot(6,6,21)
histogram2(z3_var,z4_var,'FaceColor','flat')
axis square
%xlabel('Z_3', 'FontSize', font_size)
ylabel('Z_4','FontSize', font_size)
view([0 90])
else
    return
end

if exist('z5_var','var')
subplot(6,6,25)
histogram2(z1_var,z5_var,'FaceColor','flat')
axis square
%xlabel('Z_1', 'FontSize', font_size)
ylabel('Z_5','FontSize', font_size)
view([0 90])
subplot(6,6,26)
histogram2(z2_var,z5_var,'FaceColor','flat')
axis square
%xlabel('Z_2', 'FontSize', font_size)
ylabel('Z_4','FontSize', font_size)
view([0 90])
subplot(6,6,27)
histogram2(z3_var,z5_var,'FaceColor','flat')
axis square
%xlabel('Z_3', 'FontSize', font_size)
ylabel('Z_5','FontSize', font_size)
view([0 90])
subplot(6,6,28)
histogram2(z4_var,z5_var,'FaceColor','flat')
axis square
%xlabel('Z_4', 'FontSize', font_size)
ylabel('Z_5', 'FontSize', font_size)
view([0 90])
else
    return
end

if exist('z6_var','var')
subplot(6,6,31)
histogram2(z1_var,z6_var,'FaceColor','flat')
axis square
xlabel('Z_1', 'FontSize', font_size)
ylabel('Z_6','FontSize', font_size)
view([0 90])
subplot(6,6,32)
histogram2(z2_var,z6_var,'FaceColor','flat')
axis square
xlabel('Z_2', 'FontSize', font_size)
ylabel('Z_6', 'FontSize', font_size)
view([0 90])
subplot(6,6,33)
histogram2(z3_var,z6_var,'FaceColor','flat')
axis square
xlabel('Z_3', 'FontSize', font_size)
ylabel('Z_6','FontSize', font_size)
view([0 90])
subplot(6,6,34)
histogram2(z4_var,z6_var,'FaceColor','flat')
axis square
xlabel('Z_4', 'FontSize', font_size)
ylabel('Z_6','FontSize', font_size)
view([0 90])
subplot(6,6,35)
histogram2(z5_var,z6_var,'FaceColor','flat')
axis square
xlabel('Z_5', 'FontSize', font_size)
ylabel('Z_6','FontSize', font_size)
view([0 90])
else
    return
end