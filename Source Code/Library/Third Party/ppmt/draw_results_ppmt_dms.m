%% DRAW RESULTS
close all;
figure;

L = max(simulation_ranges);

tab_names = {'Reference Distribution','PPMT Distribution',...
    'DMS Distribution','PPMT Simulations','DMS Simulations','PPMT Variogram','DMS Variogram','Time Comparison'};

desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
myGroup = desktop.addGroup('Results');
desktop.setGroupDocked('Results', 0);
myDim   = java.awt.Dimension(4, 2);   
desktop.setDocumentArrangement('Results', 1, myDim); % 1: Maximized, 2: Tiled, 3: Floating

figH    = gobjects(1, length(tab_names));
bakWarn = warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
pause(0.01);
%% REFERENCE DISTRIBUTION
iFig = 1; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002);
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
z1_var = z1_analytic;
z2_var = z2_analytic;
z3_var = z3_analytic;
z4_var = z4_analytic;
z5_var = z5_analytic;
z6_var = z6_analytic;
generate_crossplots_report;
clear z1_var z2_var z3_var z4_var z5_var z6_var;
pause(0.01);
%% PPMT Distribution
iFig = 2; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002);  % Magic, reduces rendering errors
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
if size(logs_simulated_ppmt,2)>0; z1_var = logs_simulated_ppmt(:,1); end
if size(logs_simulated_ppmt,2)>1; z2_var = logs_simulated_ppmt(:,2); end
if size(logs_simulated_ppmt,2)>2; z3_var = logs_simulated_ppmt(:,3); end
if size(logs_simulated_ppmt,2)>3; z4_var = logs_simulated_ppmt(:,4); end
if size(logs_simulated_ppmt,2)>4; z5_var = logs_simulated_ppmt(:,5); end
if size(logs_simulated_ppmt,2)>5; z6_var = logs_simulated_ppmt(:,6); end
generate_crossplots_report;
pause(0.01);

%% 3 - DMS Distribution
if exist('logs_simulated_dms','var')
    iFig = 3; 
    figH(iFig) = figure('WindowStyle', 'docked', ...
      'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
    drawnow;
    pause(0.002);  
    set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
    if size(logs_simulated_dms,1)>0; z1_var = logs_simulated_dms(1,:,:); end
    if size(logs_simulated_dms,1)>1; z2_var = logs_simulated_dms(2,:,:); end
    if size(logs_simulated_dms,1)>2; z3_var = logs_simulated_dms(3,:,:); end
    if size(logs_simulated_dms,1)>3; z4_var = logs_simulated_dms(4,:,:); end
    if size(logs_simulated_dms,1)>4; z5_var = logs_simulated_dms(5,:,:); end
    if size(logs_simulated_dms,1)>5; z6_var = logs_simulated_dms(6,:,:); end
    generate_crossplots_report;
    pause(0.01);
end
%% 4 - PPMT Output
iFig = 4;
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002); 
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
n_vars = size(logs_simulated_ppmt,2);
for p_id = 1:n_vars
    subplot(max(ceil(n_vars/3),1),min(n_vars,3),p_id);
    log_i = reshape(logs_simulated_ppmt(:,p_id), dx_sim, dy_sim);
    imagesc(log_i);
    daspect([1 1 1])
    xlabel('Y');
    ylabel('X');
    title(['Simulation of Z',num2str(p_id)]);
end
pause(0.1);

%% 5 - DMS Output
if exist('logs_simulated_dms','var')
    iFig = 5; 
    figH(iFig) = figure('WindowStyle', 'docked', ...
      'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
    drawnow;
    pause(0.002);  
    set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
    n_vars = size(logs_simulated_dms,1);
    for p_id = 1:n_vars
        subplot(max(ceil(n_vars/3),1),min(n_vars,3),p_id);
        log_i = reshape(logs_simulated_dms(p_id,:,:),dx_sim,dy_sim);

        imagesc(log_i);
        daspect([1 1 1])
        xlabel('Y');
        ylabel('X');
        title(['Simulation of Z',num2str(p_id)]);
    end
    pause(0.1);
end

%% 6 - PPMT Output Variogram
iFig = 6; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002); 
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');

    z1 = reshape(logs_simulated_ppmt(:,1),dx_sim,dy_sim);
    z2 = reshape(logs_simulated_ppmt(:,2),dx_sim,dy_sim);
    z3 = reshape(logs_simulated_ppmt(:,3),dx_sim,dy_sim);
    z4 = reshape(logs_simulated_ppmt(:,4),dx_sim,dy_sim);
    z5 = reshape(logs_simulated_ppmt(:,5),dx_sim,dy_sim);
    z6 = reshape(logs_simulated_ppmt(:,6),dx_sim,dy_sim);

    [corr_fun1,scale_X,scale_Y] = compute_experimental_correlation_function(z1, L, 1, 1);
    [corr_fun2] = compute_experimental_correlation_function(z2, L, 1, 1);
    [corr_fun3] = compute_experimental_correlation_function(z3, L, 1, 1);
    [corr_fun4] = compute_experimental_correlation_function(z4, L, 1, 1);
    [corr_fun5] = compute_experimental_correlation_function(z5, L, 1, 1);
    [corr_fun6] = compute_experimental_correlation_function(z6, L, 1, 1);

    subplot(2,3,1)
    imagesc(scale_X,scale_Y,corr_fun1)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_1')
    subplot(2,3,2)
    imagesc(scale_X,scale_Y,corr_fun2)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_2')
    subplot(2,3,3)
    imagesc(scale_X,scale_Y,corr_fun3)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_3')
    subplot(2,3,4)
    imagesc(scale_X,scale_Y,corr_fun4)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_4')
    subplot(2,3,5)
    imagesc(scale_X,scale_Y,corr_fun5)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_5')
    subplot(2,3,6)
    imagesc(scale_X,scale_Y,corr_fun6)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_6')
pause(0.1)

%% 7 - DMS Output Variogram
if exist('logs_simulated_dms','var')
    iFig = 7; 
    figH(iFig) = figure('WindowStyle', 'docked', ...
      'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
    drawnow;
    pause(0.002);  
    set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');


    z1 = reshape(logs_simulated_dms(1,:,:),dx_sim,dy_sim);
    z2 = reshape(logs_simulated_dms(2,:,:),dx_sim,dy_sim);
    z3 = reshape(logs_simulated_dms(3,:,:),dx_sim,dy_sim);
    z4 = reshape(logs_simulated_dms(4,:,:),dx_sim,dy_sim);
    z5 = reshape(logs_simulated_dms(5,:,:),dx_sim,dy_sim);
    z6 = reshape(logs_simulated_dms(6,:,:),dx_sim,dy_sim);

    [corr_fun1,scale_X,scale_Y] = compute_experimental_correlation_function(z1, L, 1, 1);
    [corr_fun2] = compute_experimental_correlation_function(z2, L, 1, 1);
    [corr_fun3] = compute_experimental_correlation_function(z3, L, 1, 1);
    [corr_fun4] = compute_experimental_correlation_function(z4, L, 1, 1);
    [corr_fun5] = compute_experimental_correlation_function(z5, L, 1, 1);
    [corr_fun6] = compute_experimental_correlation_function(z6, L, 1, 1);

    subplot(2,3,1)
    imagesc(scale_X,scale_Y,corr_fun1)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_1')
    subplot(2,3,2)
    imagesc(scale_X,scale_Y,corr_fun2)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_2')
    subplot(2,3,3)
    imagesc(scale_X,scale_Y,corr_fun3)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_3')
    subplot(2,3,4)
    imagesc(scale_X,scale_Y,corr_fun4)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_4')
    subplot(2,3,5)
    imagesc(scale_X,scale_Y,corr_fun5)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_5')
    subplot(2,3,6)
    imagesc(scale_X,scale_Y,corr_fun6)
    xlabel('\Delta x')
    ylabel('\Delta y')
    title('z_6')
end
pause(0.1)
%% 8 - Times Comparison
if exist('logs_simulated_dms','var')
    iFig = 8; 
    figH(iFig) = figure('WindowStyle', 'docked', ...
      'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
    drawnow;
    pause(0.002);  
    set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
    x = categorical({'PPMT' , 'DMS'});
	y = [time_ppmt,time_dms];
    bar(x,y)
    title('Execution Times')
    box off;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning(bakWarn);




