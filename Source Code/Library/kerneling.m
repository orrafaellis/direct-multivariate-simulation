function [ logs_k ] = kerneling( logs, n, v)
%KERNELING Summary of this function goes here
%   Detailed explanation goes here
logs_0 = logs;


logs = logs - repmat(min(logs), size(logs,1), 1);
logs = logs ./ repmat(max(logs), size(logs,1), 1);

g = v .* randn(n, size(logs,2));
index = randi(size(logs,1), n,1);

logs_k = logs(index,:) + g;

logs_k = logs_k .* repmat(max(logs_0)-min(logs_k), size(logs_k,1), 1);
logs_k = logs_k + repmat(min(logs_0), size(logs_k,1), 1);

end

