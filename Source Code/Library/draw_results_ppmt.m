%% DRAW RESULTS
close all;
figure;

L = max(simulation_ranges);

tab_names = {'Reference Distribution','PPMT Distribution',...
    'PPMT Simulations','PPMT Variogram'};

desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
myGroup = desktop.addGroup('Results');
desktop.setGroupDocked('Results', 0);
myDim   = java.awt.Dimension(4, 2);   
desktop.setDocumentArrangement('Results', 1, myDim); % 1: Maximized, 2: Tiled, 3: Floating

figH    = gobjects(1, length(tab_names));
bakWarn = warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
pause(0.01);
%% REFERENCE DISTRIBUTION
iFig = 1; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002);
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
z1_var = ref_logs(:,end-1);
z2_var = ref_logs(:,end);
generate_crossplots_report;
clear z1_var z2_var z3_var z4_var z5_var z6_var;
pause(0.01);
%% PPMT Distribution
iFig = 2; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002);  % Magic, reduces rendering errors
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
if size(logs_simulated_ppmt,2)>0; z1_var = logs_simulated_ppmt(:,1); end
if size(logs_simulated_ppmt,2)>1; z2_var = logs_simulated_ppmt(:,2); end
if size(logs_simulated_ppmt,2)>2; z3_var = logs_simulated_ppmt(:,3); end
if size(logs_simulated_ppmt,2)>3; z4_var = logs_simulated_ppmt(:,4); end
if size(logs_simulated_ppmt,2)>4; z5_var = logs_simulated_ppmt(:,5); end
if size(logs_simulated_ppmt,2)>5; z6_var = logs_simulated_ppmt(:,6); end
generate_crossplots_report;
pause(0.01);

%% 3 - PPMT Output
iFig = 3;
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002); 
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');
n_vars = size(logs_simulated_ppmt,2);
xlim=[grid_mn(1) grid_mn(1)+dx_sim*grid_size(1)];
ylim=[grid_mn(2) grid_mn(2)+dy_sim*grid_size(2)];
for p_id = 1:n_vars
    i=subplot(2,2,p_id);
    log_i = reshape(logs_simulated_ppmt(:,p_id), dx_sim, dy_sim);
    imagesc(log_i,'XData', xlim, 'YData', ylim);
    daspect([1 1 1])
    xlabel('Y');
    ylabel('X');
    title(['Simulation of Z',num2str(p_id)]);
    hold on
%     plot(ppmt_logs(:,1)+grid_size(1)/2,ppmt_logs(:,2)+grid_size(2)/2,'+k','MarkerSize',5)
    plot(ppmt_logs(:,1),ppmt_logs(:,2),'+k','MarkerSize',5)
end

for p_id = 1:n_vars
    i=subplot(2,2,2+p_id);
    log_i = reshape(ref_logs(:,2+p_id), dx_sim, dy_sim);
    imagesc(log_i,'XData', xlim, 'YData', ylim);
    daspect([1 1 1])
    xlabel('Y');
    ylabel('X');
    title(['Reference of Z',num2str(p_id)]);
    hold on
%     plot(ppmt_logs(:,1)+grid_size(1)/2,ppmt_logs(:,2)+grid_size(2)/2,'+k','MarkerSize',5)
    plot(ppmt_logs(:,1),ppmt_logs(:,2),'+k','MarkerSize',5)
end
pause(0.1);
ax11 = subplot(2,2,1);
ax21 = subplot(2,2,3);
ax12 = subplot(2,2,2);
ax22 = subplot(2,2,4);
linkaxes([ax11 ax21],'xy');
linkaxes([ax12 ax22],'xy');

%% 4 - PPMT Output Variogram
iFig = 4; 
figH(iFig) = figure('WindowStyle', 'docked', ...
  'Name', sprintf(tab_names{iFig}, iFig), 'NumberTitle', 'off');
drawnow;
pause(0.002); 
set(get(handle(figH(iFig)), 'javaframe'), 'GroupName', 'Results');

%     z1 = reshape(logs_simulated_ppmt(:,1),dx_sim,dy_sim);
%     z2 = reshape(logs_simulated_ppmt(:,2),dx_sim,dy_sim);
% 
%     [corr_fun1,scale_X,scale_Y] = compute_experimental_correlation_function(z1, L/grid_size(1), 1, 1);
%     [corr_fun2] = compute_experimental_correlation_function(z2, L/grid_size(2), 1, 1);
% 
%     subplot(1,2,1)
%     xlim = grid_size(1)*scale_X([1 end]);
%     ylim = grid_size(2)*scale_Y([1 end]);
%     imagesc(corr_fun1,'XData', xlim, 'YData',ylim);
%     xlabel('\Delta x')
%     ylabel('\Delta y')
%     title('z_1')
%     subplot(1,2,2)
%     imagesc(grid_size(2)*scale_X,grid_size(2)*scale_Y,corr_fun2)
%     xlabel('\Delta x')
%     ylabel('\Delta y')
%     title('z_2')

grid_size_transf = 0.05;
gauss_conds = [];
for var_id = 1:n_vars
    gauss_cond_i = fast_joint_ressample_no_normalization_back(logs_simulated_ppmt(:,var_id),logs_simulated_ppmt(:,var_id),grid_size_transf,1000);
    gauss_conds = [gauss_conds,gauss_cond_i];
    % Calcula variograma
    subplot(2,n_vars+1,var_id);
    variogram_field = variogram(ref_logs(:,[1 2]), gauss_cond_i,'plotit',true,'maxdist',3*simulation_ranges(var_id,1));
    % fita variograma
    subplot(2,n_vars+1,n_vars+1+var_id);
    [~,~,~,vstruct_field] = variogramfit(variogram_field.distance,variogram_field.val,[],[],[],'model','exponential');
    simulation_ranges = [simulation_ranges;vstruct_field.range,vstruct_field.range];
    pause(0.001);
end
subplot(3,n_vars+1,2*(n_vars+1))
crossvariogram_field = cross_variogram(ref_logs(:,[1 2]), gauss_conds(:,1),gauss_conds(:,2),'plotit',true,'maxdist',400);
% subplot(2,n_vars+1,2*(n_vars+1))
% [~,~,~,vstruct_field] = variogramfit(variogram_field.distance,variogram_field.val,[],[],[],'model','exponential');
pause(0.1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning(bakWarn);




