%% Visualização
font_size = 13;

subplot(2,2,1);
histogram(z1_var, 'EdgeAlpha',0)
axis square
ax = gca;
ylabel('Z_1', 'FontSize', font_size)
%%title('Z_1','FontSize',font_size,'FontWeight','normal');

%ylabel('Frequency')
grid;
if exist('z2_var','var')
    subplot(2,2,4)
    histogram(z2_var, 'EdgeAlpha',0)
    axis square
    %title('Z_1','FontSize',font_size,'FontWeight','normal');
    ylabel('Z_2', 'FontSize', font_size)
    grid;
end

if exist('z2_var','var')
subplot(2,2,3)
histogram2(z1_var,z2_var,'FaceColor','flat' , 'EdgeAlpha',0)
axis square
%xlabel('Z_1', 'FontSize', font_size)
ylabel('Z_2','FontSize', font_size)
view([0 90])
else
    return
end
