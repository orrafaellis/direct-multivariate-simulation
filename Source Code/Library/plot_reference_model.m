%load('HardData_ReferenceModel_size100_range60.mat')
cond_pos_rev = zeros(size(cond_pos));
cond_pos_rev(:,1) = cond_pos(:,2);
cond_pos_rev(:,2) = cond_pos(:,1);
figure
subplot(2,3,1)
imagesc(squeeze(reference_models(1,:,:)))
title('z_1');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

subplot(2,3,2)
imagesc(squeeze(reference_models(2,:,:)))
title('Z_2');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

if size(reference_models,1) < 3
   return 
end

subplot(2,3,3)
imagesc(squeeze(reference_models(3,:,:)))
title('Z_3');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

subplot(2,3,4)
imagesc(squeeze(reference_models(4,:,:)))
title('Z_4');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

subplot(2,3,5)
imagesc(squeeze(reference_models(5,:,:)))
title('Z_5');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

subplot(2,3,6)
imagesc(squeeze(reference_models(6,:,:)))
title('Z_6');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')