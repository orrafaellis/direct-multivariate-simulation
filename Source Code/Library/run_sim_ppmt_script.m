%% EXECUTION OF PROJECTION PERSUIT MULTIVARIATE TRANSFORMATION (PPMT)
% Connectionism and Cognitive Science Lab  
%
close all;
clc;clear;
if exist('par','dir');rmdir('par','s');mkdir('par');else mkdir('par');end 
if exist('data','dir');rmdir('data','s');mkdir('data');else mkdir('data');end 
if exist('sgsim.dbg','file');delete('sgsim.dbg');end
copyfile 'data.dat' 'data';

%% DATA PREPARATION FOR PPMT
dx_sim = 48;
dy_sim = 48;
grid_mn = [1 1]; %grid start
grid_size = [1 1]; %grid size
n_vars = 2; %number of variables to simulate
index_variables_to_sim = 1:n_vars;
variable_names = {'z1','z2'};
variable_names = variable_names(index_variables_to_sim);
ref_logs = read_eas('data/data.dat');
ref_logs(:,[1 2]) = ref_logs(:,[1 2])/10-repmat([1 0],[size(ref_logs,1) 1]);
save_table_dat('Final New Well Configuration',['Easting','Northing',variable_names],'data/data.dat', ref_logs);
%% %%%%% EXECUTE PPMT FORWARD %%%%%
ppmt_param.n_vars = n_vars;
ppmt_param.columns = 3:(n_vars+2);
ppmt_param.ppmt_par_file = 'par/ppmt_par_file.par';
ppmt_param.ref_file = 'data/data.dat';
ppmt_param.nscored_f = 'data/nscored_vars.dat';
ppmt_param.ppmt_out = 'data/ppmt.dat';
ppmt_param.ppmt_table_file = 'par/ppmt_table.trn';
generate_ppmt_par(ppmt_param);
tic
system(['"exe/ppmt.exe" "',ppmt_param.ppmt_par_file,'"']);
forward_transformation_time = toc;

condtioning_indexes = load('conditioning_indexes');
condtioning_indexes = condtioning_indexes.index_cond;
% condtioning_indexes = condtioning_indexes(1:20)
ppmt_out_original = read_eas('data/ppmt.dat');
ppmt_logs = ppmt_out_original(condtioning_indexes,:);
save_table_dat('Conditioning_Points',['Easting','Northing',variable_names],'data/ppmt.dat', ppmt_logs(:,[1:n_vars,end-n_vars+1:end]));

%variogram fitting for range estimation
figure;
simulation_ranges = [];
grid_size_transf = 0.05;
gauss_conds = [];
for var_id = 1:n_vars
    gauss_cond_i = fast_joint_ressample_no_normalization_back(ref_logs(condtioning_indexes,2+var_id),ref_logs(:,2+var_id),grid_size_transf,1000);
    gauss_conds = [gauss_conds,gauss_cond_i];
    % Calcula variograma
    subplot(2,n_vars+1,var_id);
    variogram_field = variogram(ref_logs(condtioning_indexes,[1 2]), gauss_cond_i,'plotit',true,'maxdist',400);
    % fita variograma
    subplot(2,n_vars+1,n_vars+1+var_id);
    [~,~,~,vstruct_field] = variogramfit(variogram_field.distance,variogram_field.val,[],[],[],'model','exponential');
    simulation_ranges = [simulation_ranges;vstruct_field.range,vstruct_field.range];
    pause(0.001);
end
subplot(3,n_vars+1,2*(n_vars+1))
crossvariogram_field = cross_variogram(ref_logs(condtioning_indexes,[1 2]), gauss_conds(:,1),gauss_conds(:,2),'plotit',true,'maxdist',400);
%% %%%%% EXECUTE SEQUENTIAL GAUSSIAN SIMULATION %%%%%
ppmt_out_original = read_eas(ppmt_param.ppmt_out);
ppmt_out_original = ppmt_out_original(:,end-n_vars+1:end);
for sim_id = 1:n_vars
    sgs_param = [];
    sgs_param.min = 1.1*min(ppmt_out_original(:,sim_id));
    sgs_param.max = 1.1*max(ppmt_out_original(:,sim_id));
    sgs_param.cellsx = dx_sim;
    sgs_param.cellsy = dy_sim;
    sgs_param.xmn = grid_mn;
    sgs_param.xsiz = grid_size;
    sgs_param.ymn = grid_mn;
    sgs_param.ysiz = grid_size;
    column_id = 2+sim_id;
    sgs_param.variogram_model = 2; %1.Sph; 2.Exp; 3.Gauss; 4.Power; 5.Cossine
    sgs_param.range = simulation_ranges(sim_id,:); 
    sgs_param.seed = [num2str(sim_id),'69069'];%2*(randi(9598)+randi(9598)+randi(9598))+1;
    sgs_param.search_radius = sgs_param.range*4;
    sgs_param.sgs_par_file = ['par/sgs',num2str(sim_id),'.par'];
    sgs_param.input_file = 'data/ppmt.dat';
    sgs_param.output_file = ['data/sgs',num2str(sim_id),'.dat'];
    generate_sgs_par(sgs_param,column_id);
    system(['"exe/sgsim.exe" "',sgs_param.sgs_par_file,'"']);
end

%PUT THE SIMULATIONS TOGETHER:
simulations = [];
for sim_id = 1:n_vars
    simulations = [simulations,dlmread(['data/sgs',num2str(sim_id),'.dat'],'\t',3,0)];
%      delete(['data/sgs',num2str(sim_id),'.dat']);
end
save_table_dat('Unconditional SGS Simulations',variable_names,'data/sgs.dat', simulations);

%% %%%%% EXECUTE PPMT BACK TRANSFORMATION %%%%%
ppmt_b_param.n_vars = n_vars;
ppmt_b_param.min_max = [min(min(ref_logs)),max(max(ref_logs))];
ppmt_b_param.columns = 1:n_vars;
ppmt_b_param.cellsx = dx_sim;
ppmt_b_param.cellsy = dy_sim;
ppmt_b_param.ppmtb_par_file = 'par/ppmtb_par_file.par';
ppmt_b_param.transf_table = ppmt_param.ppmt_table_file;
ppmt_b_param.input_file = 'data/sgs.dat';
ppmt_b_param.output_file = 'data/out_simulations.out';
generate_ppmtb_par(ppmt_b_param);

before_ppmtb_time = toc;
system(['"exe/ppmt_b.exe" "',ppmt_b_param.ppmtb_par_file,'"']);
total_simulation_time = toc;

logs_simulated_ppmt = read_eas(ppmt_b_param.output_file);
save_table_dat('PPMT Back Transform: Unconditional SGS Simulations',...
    variable_names,ppmt_b_param.output_file,logs_simulated_ppmt);

disp(['Time for Total Simulation: ',num2str(total_simulation_time)]);
disp(['Time for PPMT Forward Transformation: ',num2str(forward_transformation_time)]);
disp(['Time for PPMT Back Transformation: ',num2str(total_simulation_time-before_ppmtb_time)]);
%% END OF SIMULATIONS AND DATA TRANSFORMATION
% SHOW RESULTS OF PPMT
% draw_results_ppmt;
%% DMS RODANDO A MESMA SIMULA��O
%DMS Parameters -- Start
out_size_x = 48;
out_size_y = out_size_x;
grid_size = 0.05;
num_of_sims = 1;
range = simulation_ranges(:,1);
%DMS Parameters -- End
logs = ref_logs(:,end-1:end);
cond_pos = ref_logs(condtioning_indexes,[1 2]);
cond_value = ref_logs(condtioning_indexes,[3 4]);
% for i = 1:1:size(cond_pos,1)
%     for j = 1:1:size(reference_models,1)
%         cond_value(i,j) = reference_models(j,cond_pos(i,1), cond_pos(i,2));
%     end
% end
logs_simulated_dms = dms_cond(out_size_x,out_size_y, mean(range), grid_size, logs, cond_pos, cond_value, num_of_sims);
%
%