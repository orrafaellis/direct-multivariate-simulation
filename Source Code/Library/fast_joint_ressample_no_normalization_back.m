function [result] = fast_joint_ressample_no_normalization_back(logsin, logs, grid_size, num_of_samples)

noises_find = zeros(size(logsin,1),size(logs,2));

for c = 1:1:size(noises_find,1)
%    noise_cond = rand(1000, size(logs,2));
    noise_cond = repmat([0:1/num_of_samples:1]',1,size(noises_find,2));
    noise_cond = noise_cond - repmat(min(noise_cond), size(noise_cond,1), 1);
    noise_cond = noise_cond ./ repmat(max(noise_cond), size(noise_cond,1), 1);
    for j = 1:1:size(logs,2)
        


        logs_cond = fast_joint_ressample_no_normalization( noise_cond(:,1:j), logs(:,1:j), grid_size, []);

        %generate_biplots(logs_cond);
        index_noises_cond = knnsearch(logs_cond(:,1:j), logsin(c,1:j));
%         v0 = fast_joint_ressample_no_normalization( noise_cond(index_noises_cond,:), logs, grid_size, [])
%         v1 = logsin(c,:)
        noises_find(c,j) = noise_cond(index_noises_cond,j);
        noise_cond(:,j) = noises_find(c,j);
    end
end
result = noises_find;
end

