function [logs_simulated_all] = dms_cond(out_size_x,out_size_y, range, grid_size, logs, cond_pos, cond_value, num_of_sims)
if length(range)<size(logs,2)
   range = repmat(range,[1 size(logs,2)]) 
end
mmin = min(logs);
mmax = max(logs);

logs = logs - repmat(min(logs), size(logs,1), 1);
mmmax = max(logs);
logs = logs ./ repmat(mmmax, size(logs,1), 1);

krig_m = zeros(size(logs,2), out_size_x, out_size_y);
krig_v = ones(size(logs,2), out_size_x, out_size_y);
if size(cond_value, 1) > 0

    cond_value = cond_value - repmat(mmin, size(cond_value,1), 1);
    cond_value = cond_value ./ repmat(mmmax, size(cond_value,1), 1);




    % cond_value
     noises_find = fast_joint_ressample_no_normalization_back(cond_value, logs ,grid_size,1000);
    % fast_joint_ressample_no_normalization( noises_find, logs, grid_size, [])

    noises_cond2D = zeros(size(cond_pos,1),size(logs,2));
    for j = 1:1:size(logs,2)
        noises_cond2D(:,j) = noises_find(:,j);
    end


    for i = 1:1:size(krig_m,1)
        [m, v] = Kriging(cond_pos, noises_cond2D(:,i), [out_size_x,out_size_y], range(i));
        krig_m(i,:) = m(:);
        krig_v(i,:) = sqrt(v(:));
        %krig_v(i,:) = v(:);
    end
end

logs_simulated_all = cell(num_of_sims,1);

parfor n = 1:1:num_of_sims

    noises2D = zeros(size(logs,2), out_size_x, out_size_y);
    for i = 1:1:size(logs, 2)
        root_noises = randn(out_size_x * out_size_y,1);
        noises = fftma_l3c(out_size_x, out_size_y, range(i), range(i), 1, root_noises);
        noises2D(i,:,:) = noises;
    end
    
    noises = noises2D(:,:)';
    noises_cond = zeros(size(noises));
    
    for i = 1:1:size(noises2D,1)
        m = reshape(krig_m(i,:), size(noises2D,2), size(noises2D,3));
        v = reshape(krig_v(i,:), size(noises2D,2), size(noises2D,3));
        logs_to_cond = reshape(noises(:,i), size(noises2D,2), size(noises2D,3));


        logs_to_cond = (logs_to_cond - m).*v +  m;
        %logs_to_cond = (logs_to_cond).*v +  m;
        noises_cond(:,i) = reshape(logs_to_cond, out_size_x * out_size_y,1);
        index = noises_cond(:,i) > 1;
        noises_cond(index,i) = 1;
        index = noises_cond(:,i) < 0;
        noises_cond(index,i) = 0;
    end

    logs_simulated = fast_joint_ressample_no_normalization( noises_cond, logs, grid_size, []);

    logs_simulated = logs_simulated .* repmat((mmax - mmin),size(logs_simulated,1), 1);
    logs_simulated = logs_simulated + repmat(mmin,size(logs_simulated,1), 1);

    result = zeros(size(logs_simulated,2), out_size_x, out_size_y);

    for i = 1:1:size(logs_simulated,2)
        result(i,:,:) = reshape(logs_simulated(:,i), out_size_x, out_size_y);
    end
    logs_simulated_all{n} = result;
end