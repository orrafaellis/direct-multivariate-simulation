close all

%Load Experimental Data -- Start
main_nD
logs_simulated = [z1_analytic, z2_analytic, z3_analytic, z4_analytic, z5_analytic, z6_analytic];
%Load Experimental Data -- End

logs_simulated_gauss = zeros(size(logs_simulated));
transformation_table = zeros(size(logs_simulated,2), size(logs_simulated,1),2);

%Crossplot Experimental -- Start
z1_analytic = logs_simulated(:,1);
z2_analytic = logs_simulated(:,2);
z3_analytic = logs_simulated(:,3);
z4_analytic = logs_simulated(:,4);
z5_analytic = logs_simulated(:,5);
z6_analytic = logs_simulated(:,6);
generate_crossplots_leandro; %Generate Experimental Biplots
%Crossplot Experimental -- End

%Normal Score -- Start
for j = 1:1:size(logs_simulated,2)
    logs_exp = logs_simulated(:,j);
    logs_gauss = gaussianator(logs_exp);
    transformation_table(j,:,:) = [logs_exp, logs_gauss];
    logs_simulated_gauss(:,j) = logs_gauss;
end
%Normal Score -- End



%DMS -- Start

%DMS Parameters -- Start
out_size_x = 20;
out_size_y = out_size_x;
range = 10;
grid_size = 0.05;
num_of_sims = 10;
cond_pos = [15,15; 20,20];
%DMS Parameters -- End

logs_simulated_all = cell(num_of_sims,1);

for i = 1:1:num_of_sims
    logs_simulated_0 = dms(out_size_x,out_size_y, range, grid_size, logs_simulated_gauss, cond_pos , [logs_simulated_gauss(1,:);logs_simulated_gauss(1,:)]);
    logs_simulated_all{i} = logs_simulated_0;
end
%DMS -- End


%Back Transformation -- Start
logs_simulated = logs_simulated_0;
logs_simulated_back = logs_simulated_all;
for a = 1:1:size(logs_simulated_all,1)
    for b = 1:1:size(logs_simulated_all{a},1)
 
        transformation_table_gauss = reshape(transformation_table(b,:,2), size(transformation_table,2), 1);
        transformation_table_back =  reshape(transformation_table(b,:,1), size(transformation_table,2), 1)

        
            for c = 1:1:size(logs_simulated_all{a},2)
                    for d = 1:1:size(logs_simulated_all{a},3)
       
                        log_value = logs_simulated_all{a}(b,c,d);
                        index = knnsearch(transformation_table_gauss, log_value);
                        log_value_back =  transformation_table_back(index);
                        logs_simulated_back{a}(b,c,d) = log_value_back;
                        logs_simulated(b,c,d) = log_value_back;
                    end
            end
    end
end
%Back Transformation -- End

%Calculate Variances -- Start
for b = 1:1:size(logs_simulated_all{1},1)
    for c = 1:1:size(logs_simulated_all{1},2)
        for d = 1:1:size(logs_simulated_all{1},3)
            V = [];
            for a = 1:1:size(logs_simulated_all,1)
                v = logs_simulated_all{a}(b,c,d);
                V(end+1) = v;
            end
            logs_simulated_var(b,c,d) = var(V);
        end
    end
end
%Calculate Variances -- End

z1_analytic = logs_simulated(1,:);
z2_analytic = logs_simulated(2,:);
z3_analytic = logs_simulated(3,:);
z4_analytic = logs_simulated(4,:);
z5_analytic = logs_simulated(5,:);
z6_analytic = logs_simulated(6,:);

generate_crossplots_leandro %Generate Simulated Biplots
generate_2D %Generate Simulated Plot 2D Values
generate_2D_var %Generate Simulated Plot 2D Variances

