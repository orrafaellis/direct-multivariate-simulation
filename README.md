# README #

Accompanying material of "Direct Multivariate Simulation - A stepwise conditional transformation for multivariate geo statistical simulation".

### Introduction ###
This repository contains the code used to perform the simulations comparison between Direct Multivariate Simulation (DMS) and the Projection Pursuit Multivariate Transform (PPMT), given a 6-variate distribution in two dimensions. The code reproduces all  the images of subsection 4.2, according to described workflow.

### Required Software ###

MathWorks MATLAB R2018a.

### Instructions ###

To perform the simulation from inside the repository root folder in MATLAB, execute the file **run_simulations.m**. The script will load the 6-variate reference data and simulate a 200x200 grid using both DMS and PPMT. After that, all the results will be drawn.

DMS Source code and PPMT implementation can be found inside **/Source Code/**



