x = logs_simulated(:,var_index);
x = reshape(x,out_size_x,out_size_y);
if direction == 'x' x = reshape(x',1,size(x,1)*size(x,2));
else x = x(:,1);   
end
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

h.XLim = [0,50];
h.YLim = [-1,1];
hold on;

x = noises2(:,1);
x = reshape(x,out_size_x,out_size_y);
if direction == 'x' x = x(1,:);
else x = x(:,1);   
end
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);