figure 
log = reshape(logs_simulated(1,:), out_size_x, out_size_y);
cond_pos_rev = zeros(size(cond_pos));
cond_pos_rev(:,1) = cond_pos(:,2);
cond_pos_rev(:,2) = cond_pos(:,1);
subplot(2,3,1)
imagesc(log);
title('Z_1');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')


log = reshape(logs_simulated(2,:), out_size_x, out_size_y);
subplot(2,3,2)
imagesc(log);
title('Z_2');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

if size(logs_simulated,1) < 3
   return 
end

log = reshape(logs_simulated(3,:), out_size_x, out_size_y);
subplot(2,3,3)
imagesc(log);
title('Z_3');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

log = reshape(logs_simulated(4,:), out_size_x, out_size_y);
subplot(2,3,4)
imagesc(log);
title('Z_4');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

log = reshape(logs_simulated(5,:), out_size_x, out_size_y);
subplot(2,3,5)
imagesc(log);
title('Z_5');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')

log = reshape(logs_simulated(6,:), out_size_x, out_size_y);
subplot(2,3,6)
imagesc(log);
title('Z_6');
xlabel('x');
ylabel('y');
hold all
plot (cond_pos_rev(:,1),cond_pos_rev(:,2),'k+')
