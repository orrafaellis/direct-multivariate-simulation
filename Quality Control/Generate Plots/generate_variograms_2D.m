figure;
var_index = 1;
direction = 'x';
h = subplot(6,2,1);
cla
generate_variograms_2D_script;
ylabel('Z_1','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;
title('Y-Axis Covariogram');

var_index = 1;
direction = 'y';
h = subplot(6,2,2);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_1','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;
title('X-Axis Covariogram');


var_index = 2;
direction = 'x';
h = subplot(6,2,3);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_2','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;


var_index = 2;
direction = 'y';
h = subplot(6,2,4);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_2','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;


var_index = 3;
direction = 'x';
h = subplot(6,2,5);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_3','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;


var_index = 3;
direction = 'y';
h = subplot(6,2,6);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_3','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;


var_index = 4;
direction = 'x';
h = subplot(6,2,7);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_4','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;

var_index = 4;
direction = 'y';
h = subplot(6,2,8);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_4','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;


var_index = 5;
direction = 'x';
h = subplot(6,2,9);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_5','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;

var_index = 5;
direction = 'y';
h = subplot(6,2,10);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_5','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;

var_index = 6;
direction = 'x';
h = subplot(6,2,11);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_6','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;

var_index = 6;
direction = 'y';
h = subplot(6,2,12);
title('Variograms along the Y-axis');
cla
generate_variograms_2D_script;
ylabel('Z_6','FontSize',8);
xlabel('\Delta i','FontSize',8);
legend('DMS', 'FFT-MA')
box on;
grid on;
