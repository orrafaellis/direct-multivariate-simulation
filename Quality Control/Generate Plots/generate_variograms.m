figure;

%SUBPLOT%
h = subplot(6,1,1);
hold on;
cla
x = logs_simulated1(:,1);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

x = noises2(:,1);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);
h.XLim = [0, 100];
h.YLim = [-0.2, 1];
set(gca,'ytick',[0,0.5,1])
ylabel('Z_1','FontSize',15);
xlabel('\Delta t','FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
legend('DMS', 'Theorical')
box on;
grid on;


%SUBPLOT%
h = subplot(6,1,2);
hold on;
cla
x = logs_simulated1(:,2);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

x = noises2(:,2);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);
h.XLim = [0, 100];
h.YLim = [-0.2, 1];
set(gca,'ytick',[0,0.5,1])
xlabel('\Delta t','FontSize',15);
ylabel('Z_2','FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
legend('DMS', 'Theorical')
box on;
grid on;


%SUBPLOT%
h = subplot(6,1,3);
hold on;
cla
x = logs_simulated1(:,3);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

x = noises2(:,3);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);
h.XLim = [0, 100];
h.YLim = [-0.2, 1];
set(gca,'ytick',[0,0.5,1])
ylabel('Z_3','FontSize',15);
xlabel('\Delta t','FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
legend('DMS', 'Theorical')
box on;
grid on;



%SUBPLOT%
h = subplot(6,1,4);
hold on;
cla
x = logs_simulated1(:,4);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

x = noises2(:,4);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);
h.XLim = [0, 100];
h.YLim = [-0.2, 1];
set(gca,'ytick',[0,0.5,1])
ylabel('Z_4','FontSize',15);
xlabel('\Delta t','FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
legend('DMS', 'Theorical')
box on;
grid on;


%SUBPLOT%
h = subplot(6,1,5);
hold on;
cla
x = logs_simulated1(:,5);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram1 = covariogram / max(covariogram);
plot(covariogram1);

x = noises2(:,5);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);
h.XLim = [0, 100];
h.YLim = [-0.2, 1];
set(gca,'ytick',[0,0.5,1])
ylabel('Z_5','FontSize',15);
xlabel('\Delta t','FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
legend('DMS', 'Theorical')
box on;
grid on;

%SUBPLOT%
h = subplot(6,1,6);hold on;
cla
x = logs_simulated1(:,6);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);

x = noises2(:,6);
x = x(~isnan(x));
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram2 = covariogram / max(covariogram);
plot(covariogram2);

h.XLim = [0, 100];
h.YLim = [-0.2, 1];
set(gca,'ytick',[0,0.5,1])
ylabel('Z_6','FontSize',15);
xlabel('\Delta t','FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
legend('DMS', 'Theorical')
box on;
grid on;

% 
% figure; 
% hold on;
% x = logs_simulated1(:,6);
% x = x(~isnan(x));
% x= x - mean(x);
% covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
% covariogram2 = covariogram / max(covariogram);
% plot(covariogram2);
% 
% x = noises2(:,6);
% x = x(~isnan(x));
% x= x - mean(x);
% covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
% covariogram2 = covariogram / max(covariogram);
% plot(covariogram2);
% box on;
% grid on;

