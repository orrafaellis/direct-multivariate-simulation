figure;
t = [1:1:100]';
%SUBPLOT%
h = subplot(1,6,1);
plot(logs_simulated(1:100,1), t, 'black');
ylabel('t' ,'FontSize',15);
xlabel('Z_1' ,'FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
grid on;
box on;
h.YLim = [0,max(t)];
h.YDir = 'Reverse';

%SUBPLOT%
h = subplot(1,6,2);
plot(logs_simulated(1:100, 2), t, 'black');
ylabel('t' ,'FontSize',15);
xlabel('Z_2' ,'FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
grid on;
box on;
h.YLim = [0,max(t)];
h.YDir = 'Reverse';

%SUBPLOT%
h = subplot(1,6,3);

plot(logs_simulated(1:100, 3),t, 'black');
ylabel('t' ,'FontSize',15);
xlabel('Z_3' ,'FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
h.YLim = [0,max(t)];
h.YDir = 'Reverse';

%SUBPLOT%
h = subplot(1,6,4);
plot(logs_simulated(1:100, 4), t, 'black');
ylabel('t' ,'FontSize',15);
xlabel('Z_4' ,'FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
grid on;
box on;
h.YLim = [0,max(t)];
h.YDir = 'Reverse';

%SUBPLOT%
h = subplot(1,6,5);
plot(logs_simulated(1:100, 5), t, 'black');
ylabel('t' ,'FontSize',15);
xlabel('Z_5' ,'FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
grid on;
box on;
h.YLim = [0,max(t)];
h.YDir = 'Reverse';


%SUBPLOT%
h = subplot(1,6,6);
plot(logs_simulated(1:100, 6), t, 'black');
ylabel('t' ,'FontSize',15);
xlabel('Z_6' ,'FontSize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',15);
grid on;
box on;
h.YLim = [0,max(t)];
h.YDir = 'Reverse';