figure
%EXPERIMENTAL VARIOGRAM:
for var_id = 1:n_vars
    subplot(1,n_vars+1,var_id);
    variogram_field_exp = variogram(grid_(index_cond,[1 2]), ref_logs(index_cond,2+var_id),'plotit',false,'maxdist',40,'nrbins',15);
    plot(variogram_field_exp.distance,variogram_field_exp.val,'o--','color','k');
    % fita variograma
    %[~,~,~,vstruct_field] = variogramfit(variogram_field_exp.distance,variogram_field_exp.val,[],[],[],'model','exponential','color','k');
    hold on
    simulation_ranges = [simulation_ranges;vstruct_field.range,vstruct_field.range];
    pause(0.001);
end
subplot(1,n_vars+1,n_vars+1)
crossvariogram_field = cross_variogram(grid_(index_cond,[1 2]), ref_logs(index_cond,3),ref_logs(index_cond,4),'plotit',false,'maxdist',40,'nrbins',15);
plot(variogram_field_exp.distance,variogram_field_exp.val,'o--','color','k');
hold all
%[~,~,~,vstruct_field] = variogramfit(crossvariogram_field.distance,crossvariogram_field.val,[],[],[],'model','exponential','color','k');hold on

%PPMT VARIOGRAM
for i = 1:1:num_of_sims
    log_i = logs_simulated_all_ppmt{i};
    idx = randperm(length(log_i(1,:)),round(subsample_variogram_percent*length(log_i(1,:))));
    logs = [];
    for log_id = 1:n_vars
        log_ii = log_i(log_id,:,:);log_ii = log_ii(:);
        subplot(1,n_vars+1,log_id)
        variogram_field_sim_ppmt = variogram(grid_(idx,:), log_ii(idx),'plotit',false,'maxdist',40,'nrbins',15);
        pause(0.1);
        plot(variogram_field_sim_ppmt.distance,variogram_field_sim_ppmt.val,'o--','color','r');

        logs = [logs,log_ii(idx)];
        
    end
    
    % Calcula cross_variograma
    subplot(1,n_vars+1,var_id+1);
    cross_variogram_field_ppmt = cross_variogram( grid_(idx,:), logs(:,1),logs(:,2),'plotit',false,'maxdist',50);
    plot(cross_variogram_field_ppmt.distance,cross_variogram_field_ppmt.val,'o--','color','r');

    pause(0.01);
end


%DMS VARIOGRAM
for i = 1:1:num_of_sims
    log_i = logs_simulated_all_dms{i};
    idx = randperm(length(log_i(1,:)),round(subsample_variogram_percent*length(log_i(1,:))));
    logs = [];
    for log_id = 1:n_vars
        log_ii = log_i(log_id,:,:);log_ii = log_ii(:);
        subplot(1,n_vars+1,log_id)
        variogram_field_sim_dms = variogram(grid_(idx,:), log_ii(idx),'plotit',false,'maxdist',40,'nrbins',15);
        pause(0.1);
        plot(variogram_field_sim_dms.distance,variogram_field_sim_dms.val,'o--','color','b');
        logs = [logs,log_ii(idx)];
    end
    
    % Calcula cross_variograma
    s=subplot(1,n_vars+1,var_id+1);
    cross_variogram_field_dms = cross_variogram(grid_(idx,:), logs(:,1),logs(:,2),'plotit',false,'maxdist',50);
    plot(cross_variogram_field_dms.distance,cross_variogram_field_dms.val,'o--','color','b');

    pause(0.01);
end
pause(0.01);

sub1 = subplot(1,3,1);elements1 = flip(findobj(sub1,'Type','Line'));
axis([0 30 0 1.5])
legend(elements1([1 2 end]),{'Experimental' 'PPMT' 'DMS'},'Location','northwest');
grid

sub2 = subplot(1,3,2);elements2 = flip(findobj(sub2,'Type','Line'));
axis([0 30 0 1.75])
legend(elements2([1 2 end]),{'Experimental' 'PPMT' 'DMS'},'Location','northwest');
grid

sub3 = subplot(1,3,3); elements3 = flip(findobj(sub3,'Type','Line'));
legend(elements3([1 2 end]),{'Experimental' 'PPMT' 'DMS'},'Location','northwest');
axis([0 30 0 0.25])
grid