%% Visualização
figure 
subplot(6,6,1)

histogram(z1_analytic , 'EdgeAlpha',0) ;
ax = gca;
xlabel('Z_1')
ylabel('Frequency')
grid
subplot(6,6,1+7)
histogram(z2_analytic , 'EdgeAlpha',0) ;
xlabel('Z_2')
ylabel('Frequency')

grid
subplot(6,6,1+7*2)
histogram(z3_analytic , 'EdgeAlpha',0) ;
xlabel('Z_3')
ylabel('Frequency')

grid
subplot(6,6,1+7*3)
histogram(z4_analytic , 'EdgeAlpha',0) ;
xlabel('Z_4')
ylabel('Frequency')

grid
subplot(6,6,1+7*4)
histogram(z5_analytic, 'EdgeAlpha',0) ;
xlabel('Z_5')
ylabel('Frequency')

grid
subplot(6,6,1+7*5)
histogram(z6_analytic, 'EdgeAlpha',0) ;
xlabel('Z_6')
ylabel('Frequency')

grid

subplot(6,6,7)
histogram2(z1_analytic,z2_analytic,'FaceColor','flat', 'EdgeAlpha',0) ;
xlabel('Z_1')
ylabel('Z_2')
view([0 90])

subplot(6,6,13)
histogram2(z1_analytic,z3_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_1')
ylabel('Z_3')
view([0 90])
subplot(6,6,14)
histogram2(z2_analytic,z3_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_2')
ylabel('Z_3')
view([0 90])

subplot(6,6,19)
histogram2(z1_analytic,z4_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_1')
ylabel('Z_4')
view([0 90])
subplot(6,6,20)
histogram2(z2_analytic,z4_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_2')
ylabel('Z_4')
view([0 90])
subplot(6,6,21)
histogram2(z3_analytic,z4_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_3')
ylabel('Z_4')
view([0 90])


subplot(6,6,25)
histogram2(z1_analytic,z5_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_1')
ylabel('Z_5')
view([0 90])
subplot(6,6,26)
histogram2(z2_analytic,z5_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_2')
ylabel('Z_4')
view([0 90])
subplot(6,6,27)
histogram2(z3_analytic,z5_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_3')
ylabel('Z_5')
view([0 90])
subplot(6,6,28)
histogram2(z4_analytic,z5_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_4')
ylabel('Z_5')
view([0 90])

subplot(6,6,31)
histogram2(z1_analytic,z6_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_1')
ylabel('Z_6')
view([0 90])
subplot(6,6,32)
histogram2(z2_analytic,z6_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_2')
ylabel('Z_6')
view([0 90])
subplot(6,6,33)
histogram2(z3_analytic,z6_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_3')
ylabel('Z_6')
view([0 90])
subplot(6,6,34)
histogram2(z4_analytic,z6_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_4')
ylabel('Z_6')
view([0 90])
subplot(6,6,35)
histogram2(z5_analytic,z6_analytic,'FaceColor','flat' , 'EdgeAlpha',0) ;
xlabel('Z_5')
ylabel('Z_6')
view([0 90])