


t=0:1:50;
variogr = vstruct.func([vstruct.range vstruct.sill],t)';

f_corr  = max(variogr) - [ flipud(variogr (2:end)) ; variogr ];
nptos = length(f_corr);
nl2 = ((nptos-1)/2);

FFT_fcorr = abs(fft(f_corr));
FFT_filtro = sqrt(FFT_fcorr );
filtro = (ifft(FFT_filtro));
%filtro = [filtro(nl2+2:end); filtro(1:nl2+1) ];

f_corr  = max(variogr) - [variogr ; max(variogr)*ones(50,1)];
f_corr  = f_corr/max(f_corr  );
for i=1:2*length(t)+1
    for j=1:2*length(t)+1
        x = j - (length(t)+1);
        y = i - (length(t)+1);
        h = sqrt(x^2 + y^2);
        filtro_2d(i,j) = f_corr(round(h)+1);
    end
end
%filtro_2d = filtro_2d/(sum(sum(filtro_2d)));

Zhat_filt = conv2(deltas,filtro_2d,'same');